﻿namespace LendFoundry.StatusManagement
{
    public class Checklist : IChecklist
    {
        public Checklist()
        {
            
        }

        public Checklist(string code, string title, bool isCompleted)
        {
            Code = code;
            Title = title;
            IsCompleted = isCompleted;
        }

        public string Code { get; set; }
        public string Title { get; set; }
        public bool IsCompleted { get; set; }
    }
}