﻿using System.Collections.Generic;

namespace LendFoundry.StatusManagement.Configuration
{
	public class Entity
	{
		public List<Checklist> Checklist { get; set; }
		public List<Status> Statuses { get; set; }
	}
}