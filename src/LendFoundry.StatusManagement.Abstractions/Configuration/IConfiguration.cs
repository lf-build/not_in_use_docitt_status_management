﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace LendFoundry.StatusManagement.Configuration
{
    public interface IConfiguration : IDependencyConfiguration
    {
		IDictionary<string, Entity> EntityTypes { get; set; }        
    }
}