using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.StatusManagement
{
    public class EntityStatus : Aggregate, IEntityStatus
    {
	    public string EntityType { get; set; }
	    public string EntityId { get; set; }
        public string Status { get; set; }
        public string Reason { get; set; }
        public  TimeBucket ActiveOn { get; set; }
    }
}