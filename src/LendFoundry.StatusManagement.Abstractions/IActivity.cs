namespace LendFoundry.StatusManagement
{
    public interface IActivity
    {
        string Code { get; set; }
        string Title { get; set; }
    }
}