﻿namespace LendFoundry.StatusManagement
{
    public interface IChecklist
    {
        string Code { get; set; }
        string Title { get; set; }
        bool IsCompleted { get; set; }
    }
}