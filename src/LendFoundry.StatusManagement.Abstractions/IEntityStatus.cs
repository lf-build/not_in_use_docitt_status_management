using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.StatusManagement
{
    public interface IEntityStatus : IAggregate
    {
		string EntityType { get; set; }
        string EntityId { get; set; }
        string Status { get; set; }
        string Reason { get; set; }
        TimeBucket ActiveOn { get; set; }
    }
}