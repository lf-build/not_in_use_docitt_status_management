﻿namespace LendFoundry.StatusManagement
{
    public interface IReason
    {
        string Code { get; set; }
        string Descriptions { get; set; }
    }
}