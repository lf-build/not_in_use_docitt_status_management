﻿using LendFoundry.Foundation.Date;
using System.Collections.Generic;

namespace LendFoundry.StatusManagement
{
    public interface IStatus
    {
        string Code { get; set; }
        string Name { get; set; }
        string Label { get; set; }
        string Style { get; set; }
        string[] Checklist { get; set; }
        string[] Transitions { get; set; }
        Dictionary<string, string> Activities { get; set; }
        Dictionary<string, string> Reasons { get; set; }
       
    }
}