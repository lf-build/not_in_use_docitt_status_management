﻿using LendFoundry.Foundation.Date;
using System.Collections.Generic;

namespace LendFoundry.StatusManagement
{
    public interface IStatusResponse:IStatus
    {
        TimeBucket ActiveOn { get; set; }
       
    }
}