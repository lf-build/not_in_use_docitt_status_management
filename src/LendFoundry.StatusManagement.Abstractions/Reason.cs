namespace LendFoundry.StatusManagement
{
    public class Reason : IReason
    {
        public Reason()
        {
            
        }
        public Reason(string code, string descriptions)
        {
            Code = code;
            Descriptions = descriptions;
        }

        public string Code { get; set; }
        public string Descriptions { get; set; }
    }
}