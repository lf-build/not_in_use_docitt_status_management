
namespace LendFoundry.StatusManagement
{
    public interface IStatusManagementRepository
    {
        IEntityStatus GetStatus(string entityType , string entityId);
        void ChangeStatus(IEntityStatus entityStatus);
    }
}