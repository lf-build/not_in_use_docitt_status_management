using System.Collections.Generic;

namespace LendFoundry.StatusManagement
{
    public interface IEntityStatusService
    {
	    IStatusResponse GetStatusByEntity(string entityType, string entityId);
	    void ChangeStatus(string entityType, string entityId, string newStatus, string reason);
        IEnumerable<IChecklist> GetChecklist(string entityType, string entityId, string status);
        IEnumerable<ITransition> GetTransitions(string entityType, string status);
        IEnumerable<IReason> GetReasons(string entityType, string status);
        IEnumerable<IActivity> GetActivities(string entityType, string status);
    }
}