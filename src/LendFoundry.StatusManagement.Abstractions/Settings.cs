using System;

namespace LendFoundry.StatusManagement
{
    public static class Settings
    {
                public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "status-management";
    }
}