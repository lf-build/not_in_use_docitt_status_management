﻿using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;

namespace LendFoundry.StatusManagement
{
    public class StatusResponse : Status, IStatusResponse
    {
        public StatusResponse()
        {            
        }

        public StatusResponse(Status status,IEntityStatus applicationStatus) :base(status)
        {
           if(applicationStatus!=null)
            {
                ActiveOn = applicationStatus.ActiveOn;
            }
        }

     public TimeBucket ActiveOn { get; set; }

          
    }
}