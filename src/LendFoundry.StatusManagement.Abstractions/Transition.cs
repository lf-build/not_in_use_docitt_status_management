﻿namespace LendFoundry.StatusManagement
{
    public class Transition  : ITransition
    {
        public Transition()
        {
            
        }

        public Transition(string code, string name)
        {
            Code = code;
            Name = name;
        }

        public string Code { get; set; }
        public string Name { get; set; }
    }
}