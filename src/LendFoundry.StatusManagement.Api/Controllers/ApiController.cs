﻿using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif

namespace LendFoundry.StatusManagement.Api.Controllers
{
    /// <summary>
    /// ApiController class
    /// </summary>
    /// <seealso cref="ExtendedController" />
    [Route("/")]
    public class ApiController : ExtendedController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApiController"/> class.
        /// </summary>
        /// <param name="entityStatusService">The entity status service.</param>
        public ApiController(IEntityStatusService entityStatusService)
        {
            EntityStatusService = entityStatusService;
        }

        private IEntityStatusService EntityStatusService { get; }

        private static NoContentResult NoContentResult { get; } = new NoContentResult();

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <returns></returns>
        [HttpGet("{entityType}/{entityId}")]
        [ProducesResponseType(typeof(IStatusResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public IActionResult GetStatus(string entityType, string entityId)
        {
            return Execute(() => Ok(EntityStatusService.GetStatusByEntity(entityType, entityId)));
        }

        /// <summary>
        /// Changes the status.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="newStatus">The new status.</param>
        /// <param name="reason">The reason.</param>
        /// <returns></returns>
        [HttpPost("{entityType}/{entityId}/{newStatus}/{reason?}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult ChangeStatus(string entityType, string entityId, string newStatus, string reason)
        {
            try
            {
                return Execute(() =>
                {
                    EntityStatusService.ChangeStatus(entityType, entityId, newStatus, reason);
                    return NoContentResult;
                });
            }
            catch (IncompleteChecklistException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (TransitionNotAllowedException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Gets the checklist.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        [HttpGet("{entityType}/{entityId}/{status?}/checklist")]
        [ProducesResponseType(typeof(IChecklist[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public IActionResult GetChecklist(string entityType, string entityId, string status)
        {
            return Execute(() => Ok(EntityStatusService.GetChecklist(entityType, entityId, status)));
        }

        /// <summary>
        /// Gets the transitions.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        [HttpGet("{entityType}/{status}/transitions")]
        [ProducesResponseType(typeof(ITransition[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public IActionResult GetTransitions(string entityType, string status)
        {
            return Execute(() => Ok(EntityStatusService.GetTransitions(entityType, status)));
        }

        /// <summary>
        /// Gets the reasons.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        [HttpGet("{entityType}/{status}/reasons")]
        [ProducesResponseType(typeof(IReason[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public IActionResult GetReasons(string entityType, string status)
        {
            return Execute(() => Ok(EntityStatusService.GetReasons(entityType, status)));
        }

        /// <summary>
        /// Gets the activities.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        [HttpGet("{entityType}/{status}/activities")]
        [ProducesResponseType(typeof(IActivity[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public IActionResult GetActivities(string entityType, string status)
        {
            return Execute(() => Ok(EntityStatusService.GetActivities(entityType, status)));
        }
    }
}