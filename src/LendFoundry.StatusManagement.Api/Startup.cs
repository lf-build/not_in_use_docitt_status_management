﻿using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;

using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.StatusManagement.Configuration;
using LendFoundry.StatusManagement.Persistence;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.ServiceDependencyResolver;

#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
#endif
using System;
using System.Runtime;
using System.Collections.Generic;
using LendFoundry.Configuration;

namespace LendFoundry.StatusManagement.Api
{
    internal class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            // Register the Swagger generator, defining one or more Swagger documents
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "StatusManagement"
                });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
               {
                   { "Bearer", new string[] { } }
               });

                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LendFoundry.StatusManagement.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#else
            services.AddSwaggerDocumentation();
#endif

            // services
            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddTenantService();
            services.AddDecisionEngine();
            services.AddLookupService();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddConfigurationService<Configuration.Configuration>(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<Configuration.Configuration>(Settings.ServiceName);
            services.AddEventHub(Settings.ServiceName);

            // interface implements
            services.AddMongoConfiguration(Settings.ServiceName);
            services.AddTransient<IConfiguration>(provider => provider.GetRequiredService<IConfigurationService<Configuration.Configuration>>().Get());
            services.AddTransient<IEntityStatusService, StatusManagementService>();
            services.AddTransient<IStatusManagementRepository, StatusManagementRepository>();

            // aspnet
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
            Console.WriteLine(GCSettings.LatencyMode);
            Console.WriteLine(GCSettings.IsServerGC);
            Console.WriteLine(GCSettings.LargeObjectHeapCompactionMode);

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "StatusManagement Service");
            });

#endif

            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();

            app.UseConfigurationCacheDependency();

        }
    }
}