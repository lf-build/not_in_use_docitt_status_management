﻿using LendFoundry.Foundation.Client;
using RestSharp;
using System.Collections.Generic;

namespace LendFoundry.StatusManagement.Client
{
    public class StatusManagementServiceClient : IEntityStatusService
    {
        public StatusManagementServiceClient(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public IStatusResponse GetStatusByEntity(string entityType, string entityId)
        {
             return Client.GetAsync<StatusResponse>($"/{entityType}/{entityId}").Result;
        }

        public void ChangeStatus(string entityType, string entityId, string newStatus, string reason)
        {
            if (!string.IsNullOrWhiteSpace(reason))
                Client.PostAsync<dynamic>($"/{entityType}/{entityId}/{newStatus}/{reason}", null, true);                 
            else
                Client.PostAsync<dynamic>($"/{entityType}/{entityId}/{newStatus}", null, true);
        }

        public IEnumerable<IChecklist> GetChecklist(string entityType, string entityId, string status)
        {
            var request = new RestRequest("/{entityType}/{entityId}/{status?}/checklist", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);

            if (!string.IsNullOrWhiteSpace(status))
                request.AddUrlSegment("status", status);

            return Client.Execute<List<Checklist>>(request);
        }

        public IEnumerable<ITransition> GetTransitions(string entityType, string status)
        {
            return  Client.GetAsync<List<Transition>>($"/{entityType}/{status}/transitions").Result;
        }

        public IEnumerable<IReason> GetReasons(string entityType, string status)
        {
            return  Client.GetAsync<List<Reason>>($"/{entityType}/{status}/reasons").Result;
        }

        public IEnumerable<IActivity> GetActivities(string entityType, string status)
        {
            return  Client.GetAsync<List<Activity>>($"/{entityType}/{status}/activities").Result;
        }
    }
}
