﻿using System.Linq;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson;

namespace LendFoundry.StatusManagement.Persistence
{
    public class StatusManagementRepository : MongoRepository<IEntityStatus,EntityStatus> ,IStatusManagementRepository
    {
        static StatusManagementRepository()
        {
            BsonClassMap.RegisterClassMap<EntityStatus>(map =>
            {
                map.AutoMap();
                var type = typeof(EntityStatus);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");            
                map.SetIsRootClass(true);
            });
        }

        public StatusManagementRepository(ITenantService tenantService, IMongoConfiguration configuration) : base(tenantService, configuration, "status-management")
        {
            CreateIndexIfNotExists("entity-type-id", Builders<IEntityStatus>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType).Ascending(i => i.EntityId));
        }
		
		public IEntityStatus GetStatus(string entityType, string entityId)
		{
			return Query.FirstOrDefault(a => a.EntityType == entityType && a.EntityId == entityId);
		}

		public void ChangeStatus(IEntityStatus entityStatus)
        {
            entityStatus.TenantId = TenantService.Current.Id;

            Collection.UpdateOne(s =>
					s.EntityType == entityStatus.EntityType &&
                    s.EntityId == entityStatus.EntityId &&
                    s.TenantId == TenantService.Current.Id,
                new UpdateDefinitionBuilder<IEntityStatus>()
                    .Set(a => a.Status, entityStatus.Status)
                    .Set(a => a.Reason, entityStatus.Reason)
                    .Set(a => a.ActiveOn,entityStatus.ActiveOn)
                , new UpdateOptions() {IsUpsert = true});
        }

    }
}
