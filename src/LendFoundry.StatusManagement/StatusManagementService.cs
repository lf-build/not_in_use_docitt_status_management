﻿using LendFoundry.Clients.DecisionEngine;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.StatusManagement.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace LendFoundry.StatusManagement
{
    public class StatusManagementService : IEntityStatusService
    {
        public StatusManagementService
        (
            IDecisionEngineService decisionEngine,
            IStatusManagementRepository entityStatusRepository,
            IConfiguration configuration,
            IEventHubClient eventHubClient,
            ILookupService lookup,
            ITenantTime tenantTime,
            ILogger logger
        )
        {
            if (decisionEngine == null) throw new ArgumentException($"{nameof(decisionEngine)} is mandatory");
            if (entityStatusRepository == null) throw new ArgumentException($"{nameof(entityStatusRepository)} is mandatory");
            if (configuration == null) throw new ArgumentException($"{nameof(configuration)} is mandatory");
            if (eventHubClient == null) throw new ArgumentException($"{nameof(eventHubClient)} is mandatory");
            if (lookup == null) throw new ArgumentException($"{nameof(lookup)} is mandatory");
            if (tenantTime == null) throw new ArgumentException($"{nameof(tenantTime)} is mandatory");
            if (logger == null) throw new ArgumentException($"{nameof(logger)} is mandatory");

            DecisionEngine = decisionEngine;
            EntityStatusRepository = entityStatusRepository;
            Configuration = configuration;
            EventHubClient = eventHubClient;
            Lookup = lookup;
            TenantTime = tenantTime;
            Logger = logger;
        }

        private ILookupService Lookup { get; }

        public IDecisionEngineService DecisionEngine { get; }

        private IStatusManagementRepository EntityStatusRepository { get; }

        private IConfiguration Configuration { get; }

        private IEventHubClient EventHubClient { get; }

        private ITenantTime TenantTime { get; }

        private ILogger Logger { get; set; }

        public IStatusResponse GetStatusByEntity(string entityType, string entityId)
        {
            try
            {
                entityType = EnsureInputIsValid(entityType, entityId);

                var applicationStatus = EntityStatusRepository.GetStatus(entityType, entityId);

                if (applicationStatus == null)
                    throw new NotFoundException($"Application type {entityType} Or Application Id {entityId} was not found.");

                var status = GetStatusByCode(entityType, applicationStatus.Status);
                var statusResponse = new StatusResponse(status, applicationStatus);

                return statusResponse;
            }
            catch (Exception ex)
            {
                Logger.Error($"Occurred an error when try to get status by entity ({entityType}, {entityId}) : {ex.Message}\n", ex);
                throw ex;
            }
        }

        public void ChangeStatus(string entityType, string entityId, string newStatus, string reason)
        {
            try
            {
                entityType = EnsureInputIsValid(entityType, entityId, newStatus);

                var entityStatus = EntityStatusRepository.GetStatus(entityType, entityId);
                Status oldStatus = null;

                if (entityStatus != null)
                {
                    oldStatus = GetStatusByCode(entityType, entityStatus.Status);
                    var newStatusEntity = GetStatusByCode(entityType, newStatus);

                    if (!oldStatus.Transitions.Contains(newStatus))
                        throw new TransitionNotAllowedException();

                    if (newStatusEntity.Reasons.Any())
                    {
                        if (string.IsNullOrWhiteSpace(reason))
                            throw new TransitionNotAllowedException("Reason is required");

                        if (!newStatusEntity.Reasons.ContainsKey(reason))
                            throw new TransitionNotAllowedException($"Reason {reason} is not valid");
                    }

                    var checkList = GetChecklist(entityType, entityId, newStatusEntity.Code);

                    if (checkList != null && checkList.Any(c => !c.IsCompleted))
                        throw new IncompleteChecklistException();
                }

                var todayDate = new TimeBucket(TenantTime.Now);
                EntityStatusRepository.ChangeStatus(new EntityStatus
                {
                    EntityType = entityType,
                    EntityId = entityId,
                    Reason = reason,
                    Status = newStatus,
                    ActiveOn = todayDate
                });

                var newStatusName = Configuration.EntityTypes[entityType].Statuses.FirstOrDefault(x => x.Code == newStatus)?.Name;
                var newLabelName = Configuration.EntityTypes[entityType].Statuses.FirstOrDefault(x => x.Code == newStatus)?.Label;
                var eventName = UppercaseFirst($"{entityType}StatusChanged");
                EventHubClient.Publish(eventName, new StatusChanged
                {
                    EntityType = entityType,
                    EntityId = entityId,
                    OldStatus = oldStatus?.Code,
                    NewStatus = newStatus,
                    NewStatusName = newStatusName,
                    NewStatusLabel = newLabelName,
                    Reason = reason,
                    ActiveOn = todayDate
                });
            }
            catch (Exception ex)
            {
                Logger.Error($"Occurred an error when try to change status ({entityType}, {entityId}, {newStatus}, {reason}) : {ex.Message}\n", ex);
                throw ex;
            }
        }

        public IEnumerable<IChecklist> GetChecklist(string entityType, string entityId, string statusCode)
        {
            try
            {
                entityType = EnsureInputIsValid(entityType, entityId);

                var applicationStatus = GetStatusByEntity(entityType, entityId);
                var requestedStatus = statusCode;

                if (string.IsNullOrWhiteSpace(requestedStatus))
                    requestedStatus = applicationStatus.Code;

                var status = GetStatusByCode(entityType, requestedStatus);

                return GetChecklist(entityType, entityId, status);
            }
            catch (Exception ex)
            {
                Logger.Error($"Occurred an error when try to get check list ({entityType}, {entityId}, {statusCode}) : {ex.Message}\n", ex);
                throw ex;
            }
        }

        public IEnumerable<ITransition> GetTransitions(string entityType, string status)
        {
            try
            {
                entityType = EnsureInputIsValid(entityType, "entityId", status);
                var statusDetail = GetStatusByCode(entityType, status);

                if (statusDetail.Transitions == null || !statusDetail.Transitions.Any())
                    throw new NotFoundException($"Transitions not found for status {status}");

                var entity = Configuration.EntityTypes.FirstOrDefault(a => a.Key.Equals(entityType)).Value;

                return entity.Statuses.Where(
                    s => statusDetail.Transitions.Contains(s.Code, StringComparer.InvariantCultureIgnoreCase))
                    .Select(s => new Transition(s.Code, s.Name));
            }
            catch (Exception ex)
            {
                Logger.Error($"Occurred an error when try to get transitions ({entityType}, {status}) : {ex.Message}\n", ex);
                throw ex;
            }
        }

        public IEnumerable<IReason> GetReasons(string entityType, string status)
        {
            try
            {
                entityType = EnsureInputIsValid(entityType, "entityId", status);

                var statusDetail = GetStatusByCode(entityType, status);

                if (statusDetail.Reasons == null || !statusDetail.Reasons.Any())
                    throw new NotFoundException($"Reasons not found for status {status}");

                return statusDetail.Reasons.Select(r => new Reason(r.Key, r.Value));
            }
            catch (Exception ex)
            {
                Logger.Error($"Occurred an error when try to get reasons ({entityType}, {status}) : {ex.Message}\n", ex);
                throw ex;
            }
        }

        public IEnumerable<IActivity> GetActivities(string entityType, string status)
        {
            try
            {
                entityType = EnsureInputIsValid(entityType, "entityId", status);

                var statusDetail = GetStatusByCode(entityType, status);

                if (statusDetail.Activities == null || !statusDetail.Activities.Any())
                    throw new NotFoundException($"Activities not found for status {status}");

                return statusDetail.Activities.Select(a => new Activity(a.Key, a.Value));
            }
            catch (Exception ex)
            {
                Logger.Error($"Occurred an error when try to get activities ({entityType}, {status}) : {ex.Message}\n", ex);
                throw ex;
            }
        }

        private Status GetStatusByCode(string entityType, string code)
        {
            entityType = EnsureInputIsValid(entityType, "entityId", code);

            var status = Configuration.EntityTypes
                .FirstOrDefault(a => a.Key == entityType)
                .Value
                .Statuses
                .FirstOrDefault(s => string.Equals(s.Code, code, StringComparison.InvariantCultureIgnoreCase));

            if (status == null)
                throw new ArgumentException($"Invalid status '{code}'");

            return new Status(status);
        }

        private IEnumerable<IChecklist> GetChecklist(string entityType, string entityId, Status status)
        {
            entityType = EnsureInputIsValid(entityType, entityId);

            var checklist = new ConcurrentBag<IChecklist>();
            var entity = Configuration.EntityTypes.FirstOrDefault(a => a.Key == entityType).Value;

            if (status.Checklist == null)
                return checklist;

            var checklistDefinitions = entity
                .Checklist
                .Where(c => status.Checklist.Contains(c.Code, StringComparer.InvariantCultureIgnoreCase));

            Parallel.ForEach(checklistDefinitions, item =>
            {
                try
                {
                    var isCompleted = DecisionEngine.Execute<dynamic, bool>(item.RuleName, new
                    {
                        payload = new { entityId }
                    });

                    checklist.Add(new Checklist(item.Code, item.Title, isCompleted));
                }
                catch (Exception ex)
                {
                    checklist.Add(new Checklist(item.Code, item.Title, false));
                    Logger.Error($"Rule {item.RuleName} could not be executed, please see decision-engine logs for more information.", ex);
                }
            });

            return checklist.OrderBy(c => c.Code);
        }

        private static string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s)) return string.Empty;
            return $"{char.ToUpper(s[0])}{s.Substring(1)}";
        }

        private string EnsureInputIsValid(string entityType, string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory");

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");
            entityType = entityType.ToLower();
            if (Lookup.GetLookupEntry("entityTypes", entityType) == null)
                throw new ArgumentException($"{entityType} is not a valid entity");
            return entityType;
        }

        private string EnsureInputIsValid(string entityType, string entityId, string status)
        {
            entityType = EnsureInputIsValid(entityType, entityId);

            if (!Configuration.EntityTypes.ContainsKey(entityType))
                throw new NotFoundException($"Entity {nameof(entityType)} not found in configuration");

            if (!Configuration.EntityTypes.FirstOrDefault(a => a.Key == entityType).Value
                               .Statuses.Any(s => string.Equals(s.Code, status, StringComparison.InvariantCultureIgnoreCase)))
                throw new NotFoundException($"Status {nameof(entityType)} not found in configuration");
            return entityType;
        }
    }
}