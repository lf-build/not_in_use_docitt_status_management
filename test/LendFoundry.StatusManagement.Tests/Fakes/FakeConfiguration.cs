﻿using LendFoundry.StatusManagement.Configuration;
using System.Collections.Generic;

namespace LendFoundry.StatusManagement.Tests.Fakes
{
    public class FakeConfiguration : IConfiguration
    {
        public List<Checklist> Checklist { get; set; }
        public List<Status> Statuses { get; set; }
        public IDictionary<string, Entity> EntityTypes { get; set; }
        public string LookupEntityNameKey { get; set; }
    }
}
