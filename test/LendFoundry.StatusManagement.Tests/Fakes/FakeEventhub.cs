﻿using LendFoundry.EventHub.Client;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.StatusManagement.Tests.Fakes
{
    public class FakeEventHub : IEventHubClient
    {
        public void PublishBatchWithInterval<T>(List<T> events, int interval = 100)
        {
            throw new NotImplementedException();
        }

        public void On(string eventName, Action<EventInfo> handler)
        {
            handler.Invoke(new EventInfo { });
        }

        public Task<bool> Publish<T>(T @event)
        {
            return Task.Run(() => true);
        }

        public Task<bool> Publish<T>(string eventName, T @event)
        {
            return Task.Run(() => true);
        }

        public void Start()
        {
           
        }

        public void StartAsync()
        {
            
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }
    }
}
