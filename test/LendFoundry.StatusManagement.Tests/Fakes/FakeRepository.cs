﻿using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.StatusManagement.Tests.Fakes
{
    public class FakeRepository : IStatusManagementRepository
    {
        public List<IEntityStatus> InMemoryData { get; set; }
            = new List<IEntityStatus>();

	    public IEntityStatus GetStatus(string entityType, string entityId)
	    {
		    return InMemoryData.FirstOrDefault(a => a.EntityType.Equals(entityType) && a.EntityId.Equals(entityId));
	    }

	    public void ChangeStatus(IEntityStatus entityStatus)
        {
          
        }

        public IEntityStatus GetStatus(string entityId)
        {
            return InMemoryData.FirstOrDefault(x => x.EntityId == entityId);
        }
        
    }
}
