﻿using LendFoundry.Foundation.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RestSharp;
using Moq;

namespace LendFoundry.StatusManagement.Tests.Fakes
{
    public class FakeServiceClient<TData> : IServiceClient
    {
        public object InMemoryData { get; set; }

        public bool Execute(IRestRequest request)
        {
            return default(bool);
        }

        public object Execute(IRestRequest request, Type type)
        {
            throw new NotImplementedException();
        }

        public T Execute<T>(IRestRequest request)
        {
            return (T)InMemoryData;
        }

        public Task<bool> ExecuteAsync(IRestRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<object> ExecuteAsync(IRestRequest request, Type type)
        {
            throw new NotImplementedException();
        }

        public Task<T> ExecuteAsync<T>(IRestRequest request)
        {
            throw new NotImplementedException();
        }

        public IRestResponse ExecuteRequest(IRestRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<IRestResponse> ExecuteRequestAsync(IRestRequest request)
        {
            throw new NotImplementedException();
        }
    }
}
